name := "CS441HW2"

version := "0.1"

scalaVersion := "2.11.0"

scalacOptions += "-target:jvm-1.7"
javacOptions in (Compile, compile) ++= Seq("-source", "1.7", "-target", "1.7", "-g:lines")

assemblyMergeStrategy in assembly := { case PathList("META-INF", xs @ _*) => MergeStrategy.discard

case x => MergeStrategy.first }

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.2.1"
)
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"

libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.2.0"
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.1.1"

libraryDependencies += "com.novocode" % "junit-interface" % "0.8" % "test->default"
testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")