package com.cs441.hw2

//Unit Tests for MapReduce program
//Submitted by: Joylyn Lewis

//Import required libraries
import java.io.InputStream

import org.junit.Assert._
import org.junit.Test
import scala.io.Source
import scala.xml.XML
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

class HW2_MapReduceTest {
  //define config variable to read input values
  val config =  com.typesafe.config.ConfigFactory.load("application.conf")

  //Unit Test 1: Test whether UIC CS Professor values are read correctly
  @Test
  def uicProfTest : Unit = {

    val uicCsProfsFile = Source.fromFile("./src/main/resources/uic_cs_profs.txt")

    val lines = uicCsProfsFile.getLines()                                                                                           //Read the lines from the input CS UIC Professors file
    val uicCSProfsFile = lines.filter(f => !f.trim.isEmpty)
    val uicCsProfsMap = uicCSProfsFile.map(m2 => (m2.split(":")(0), m2.split(":")(1))).toMap                        //Generate map of UIC CS professors with key as names appearign in DBLP and value as names appearing in the UIC faculty site
    assertTrue(uicCsProfsMap("Tanya Y. Berger-Wolf") == "Tanya Berger-Wolf")
  }

  //Unit Test 2: Test whether UIC author parsed from XML correctly
  @Test
  def xmlParseAuthor : Unit = {
    val xmlDoc = XML.loadString("<article mdate=\"2017-05-28\" key=\"journals/acta/Saxena96\">\n<author>Mark Grechanik</author>\n<author>Isabel F. Cruz</author>\n<author>Ugo Buy</author>\n<title>Parallel Integer Sorting and Simulation Amongst CRCW Models.</title>\n<pages>607-619</pages>\n<year>1996</year>\n<volume>33</volume>\n<journal>Acta Inf.</journal>\n<number>7</number>\n<url>db/journals/acta/acta33.html#Saxena96</url>\n<ee>https://doi.org/10.1007/BF03036466</ee>\n</article>")
    val authorsArticleNodes = (xmlDoc \ config.getString("MapReduce.authorTag")).map(author => author.text).toList;         //Parse the XML content to obtain list of authors for the publication
    assertTrue(authorsArticleNodes.contains("Mark Grechanik" ))

  }

  //Unit Test 3: Test whether values can be read correctly from the input file 'application.conf'
  @Test
  def ConfigAuthorTest: Unit = {
    val author = config.getString("MapReduce.authorTag")
    assertTrue(author == "author")
  }

  //Unit Test 4: Test whether logger object is created successfully
  @Test
  def loggerTest : Unit = {
    val logger = Logger(LoggerFactory.getLogger(config.getString("MapReduce.logger")))
    assertNotNull(logger)

  }

  //Unit Test 5: Test whether number of pairs of professors are generated correctly
  @Test
  def authorPairs : Unit = {
    val xmlDoc = XML.loadString("<article mdate=\"2017-05-28\" key=\"journals/acta/Saxena96\">\n<author>Mark Grechanik</author>\n<author>Isabel F. Cruz</author>\n<author>Ugo Buy</author>\n<title>Parallel Integer Sorting and Simulation Amongst CRCW Models.</title>\n<pages>607-619</pages>\n<year>1996</year>\n<volume>33</volume>\n<journal>Acta Inf.</journal>\n<number>7</number>\n<url>db/journals/acta/acta33.html#Saxena96</url>\n<ee>https://doi.org/10.1007/BF03036466</ee>\n</article>")
    val authorsArticleNodes = (xmlDoc \ config.getString("MapReduce.authorTag")).map(author => author.text).toList;                   //Parse the XML content to obtain list of authors for the publication
    val pairs = authorsArticleNodes.flatMap(x => authorsArticleNodes.map(y => (x, y)))                                                       //Generate list of tuples where each tuple is a pair of professors who worked on the publication

    assertTrue(pairs.length == 9)

  }

}
