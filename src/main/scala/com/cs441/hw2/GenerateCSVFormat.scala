package com.cs441.hw2

//This program is only to generate the CSV format of the output written by the MapReduce program

import java.io.PrintWriter
import com.typesafe.config._
import scala.io.Source

object GenerateCSVFormat {
  def main(args: Array[String]): Unit = {

    val config= ConfigFactory.load("application.conf")
    val outputData = getClass.getResourceAsStream(config.getString("CSVFileFornat.mapReduceOutput"))

    val linesText = scala.io.Source.fromInputStream(outputData).getLines()

    val output = linesText.filter(f=> !f.trim.isEmpty)
    val OutputMap = output.map(m2=> (m2.split("\t")(0), m2.split("\t")(1))).toMap

    val outputFile = new PrintWriter(config.getString("CSVFileFornat.OutputCSVFileName"))
    outputFile.write(config.getString("CSVFileFornat.OutputCSVHeader"))

    OutputMap.foreach(pair =>  {
      val weight = OutputMap.get(pair._2)
      outputFile.write((pair._1 + '\n') * pair._2.toInt)
    })

    outputFile.close()
  }





}
