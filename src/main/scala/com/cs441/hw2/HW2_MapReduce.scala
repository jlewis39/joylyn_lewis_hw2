package com.cs441.hw2

//CS 441 HW2: Implementing MapReduce model for processing DBLP dataset
//Submitted by: Joylyn Lewis

//Description: This program provides the implementation of the mapper and reducer along with the XMLInputFormat required to parse XML files. The processing is done on the DBLP dataset to capture
//associations between UIC CS professors who have co-authored a publication. The processing also captures number of publications written by each UIC CS professor.

//Import required libraries
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.IntWritable
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.mapreduce.{Job, Reducer}
import scala.collection.JavaConverters._
import java.io._
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.DataOutputBuffer
import org.apache.hadoop.mapreduce.InputSplit
import org.apache.hadoop.mapreduce.RecordReader
import org.apache.hadoop.mapreduce.TaskAttemptContext
import org.apache.hadoop.mapreduce.lib.input.FileSplit
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.mapreduce.Mapper
import scala.xml.XML
import com.typesafe.config._
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger

object HW2_MapReduce {

  //Class UICAuthorsMapper represents the implementation of the mapper
  class UICAuthorsMapper extends Mapper[LongWritable, Text, Text, IntWritable] {

    val config= ConfigFactory.load("application.conf")                                                                //Load the application's configuration from the classpath resource basename
    val one = new IntWritable( config.getString("MapReduce.one").toInt)                                                         //Represents the value 1 for each association between UIC CS professors who co-authored publication

    //Map method processes one publication at a time and emits key/value pairs in the format <Author1,Author2 1>
    override def map(key: LongWritable, value: Text, context: Mapper[LongWritable, Text, Text, IntWritable]#Context): Unit = {

      try{

        val uicCsProfsFile: InputStream = getClass.getResourceAsStream(config.getString("MapReduce.uicCsProfsFile"))            //Load the application's configuration from the classpath resource basename
        val lines = scala.io.Source.fromInputStream(uicCsProfsFile).getLines()                                                         //Read the lines from the input CS UIC Professors file
        val uicCSProfsFile = lines.filter(f => !f.trim.isEmpty)
        val uicCsProfsMap = uicCSProfsFile.map(m2 => (m2.split(":")(0), m2.split(":")(1))).toMap                        //Generate map of UIC CS professors with key as names appearign in DBLP and value as names appearing in the UIC faculty site

        val xmlDoc = XML.loadString(value.toString)                                                                                    //Load the XML content to be worked on by the mapper

        val authorsArticleNodes = (xmlDoc \ config.getString("MapReduce.authorTag")).map(author => author.text).toList;         //Parse the XML content to obtain list of authors for the publication
        val uicAuthorsList = authorsArticleNodes.filter(uicCsProfsMap.contains(_))                                                     //Filter the list of authors to obtain only UIC CS professors
        val pairs = uicAuthorsList.flatMap(x => uicAuthorsList.map(y => (x, y)))                                                       //Generate list of tuples where each tuple is a pair of professors who worked on the publication

        pairs.foreach(tuple => {

          context.write(new Text(tuple._1 + "," + tuple._2), one)                                                                     //Write each pair of professors as the key and 1 which represents the value of association between the pair of professors

        })

      } catch {
        case e: Exception => e.printStackTrace
          val logger = Logger(LoggerFactory.getLogger(config.getString("MapReduce.logger")))
          logger.error("The simulation has been terminated due to an unexpected error")                                               //Log error in case of exception
      }

    }

  }

  //Class UICAuthorsReducer represents the implementation of the reducer
  class UICAuthorsReducer extends Reducer[Text, IntWritable, Text, IntWritable] {

    //Method reduce processes each pair written by mapper, adding the counts for the matching pair of professors in the key/value pair written by mapper
    override def reduce(key: Text, values: java.lang.Iterable[IntWritable], context: Reducer[Text, IntWritable, Text, IntWritable]#Context): Unit = {

      try{
        val sum  = values.asScala.foldLeft(0)(_ + _.get)                                                                              //Add the associations for matching pairs of professors
        context.write(key, new IntWritable(sum))                                                                                      //Write output as key -> pair of professors and value -> sum of associations between the pair of professors

      } catch {
        case e: Exception => e.printStackTrace
          val config= ConfigFactory.load("application.conf")                                                         //Load the application's configuration from the classpath resource basename
          val logger = Logger(LoggerFactory.getLogger(config.getString("MapReduce.logger")))
          logger.error("The simulation has been terminated due to an unexpected error")                                               //Log error in case of exception
      }

    }

  }

  //Class XMLInputFormat is used for parsing the input XML
  class XmlInputFormat extends org.apache.hadoop.mapreduce.lib.input.TextInputFormat {

    //create record reader for reading xml records
    override def createRecordReader(split: InputSplit, context: TaskAttemptContext): RecordReader[LongWritable, Text] = {
      new XmlRecordReader()
    }
  }

  //XMLRecordReader class reads through a given XML document to output xml blocks as records specified by start and end tags
  class XmlRecordReader extends RecordReader[LongWritable, Text] {

    //Mutable variables are required here as these variables will be used throughout the functions of class XmlRecordReader to read through the XML content
    var startTag : Array[Byte] =_
    var endTag: Array[Byte] = _
    var start: Long = _
    var end: Long = _
    var fsin : FSDataInputStream = _
    private var buffer : DataOutputBuffer = new DataOutputBuffer()
    private var key: LongWritable = new LongWritable()
    private var value: Text = new Text()

    val config= ConfigFactory.load("application.conf")                                                                //Load the application's configuration from the classpath resource basename
    var START_TAG_KEY: String = config.getString("MapReduce.startTagKey")                                                       //Obtain the start and end tag keys
    var END_TAG_KEY: String = config.getString("MapReduce.endTagKey")

    override def initialize(split: InputSplit, context: TaskAttemptContext): Unit = {
      val conf: Configuration = context.getConfiguration()
      val fileSplit = split.asInstanceOf[FileSplit]
      val START_TAG_KEY: String = config.getString("MapReduce.startTag")                                                        //Obtain the start and end tags
      val END_TAG_KEY: String = config.getString("MapReduce.endTag")

      startTag = START_TAG_KEY.getBytes(config.getString("MapReduce.charSet"))                                                 //Set the UTF-8 encoding scheme
      endTag = END_TAG_KEY.getBytes(config.getString("MapReduce.charSet"))

      start = fileSplit.getStart()                                                                                                    //open the file and seek to start of split
      end = start + fileSplit.getLength()
      val file : Path = fileSplit.getPath()

      val fs : FileSystem = file.getFileSystem(conf)
      fsin = fs.open(fileSplit.getPath())
      fsin.seek(start)
    }

    override def nextKeyValue: Boolean = {

      val config= ConfigFactory.load("application.conf")                                                             //Load the application's configuration from the classpath resource basename
      val logger = Logger(LoggerFactory.getLogger(config.getString("MapReduce.logger")))

      if(fsin.getPos() < end){
        if (readUntilMatch(startTag, false)) {                                                                            //Obtain the next XML record
          try {
            val dtdFilePath = getClass.getClassLoader.getResource(config.getString("MapReduce.dtdFile")).toURI

            //Providing the exact header string here in order to avoid errors during parsing
            val XMLString : String = s"""<?xml version="1.0" encoding="ISO-8859-1"?>
                    <!DOCTYPE dblp SYSTEM "$dtdFilePath">"""

            buffer.writeBytes(XMLString)
            buffer.write(startTag);

            if (readUntilMatch(endTag, true)) {
              key.set(fsin.getPos());
              value.set(buffer.getData(), 0, buffer.getLength());

              return true;
            }
          } catch {
            case e: Exception => e.printStackTrace
              logger.error("The simulation has been terminated due to an unexpected error")                                          //Log error in case of exception
          }

          finally {
            buffer.reset();
          }
        }
      }
      return false
    }

    override def getCurrentKey: LongWritable = key

    override def getCurrentValue: Text = value

    override def getProgress: Float =
    { val difference : Float = end - start
      return ((fsin.getPos() - start) /  difference)
    }

    override def close: Unit = fsin.close()

    def readUntilMatch(matchWord : Array[Byte] , withinBlock : Boolean) : Boolean = {                                                       //Read XML content until match record
      var i: Int = 0
      while(true){
        val b = fsin.read()
        //end of file
        if (b == -1)
          return false
        //save to buffer
        if(withinBlock)
          buffer.write(b)
        //check if matching
        if(b == matchWord(i)){
          i += 1
          if(i >= matchWord.length)
            return true
        } else
          i = 0

        if(!withinBlock && i == 0 && fsin.getPos() >= end)
          return false

      }
      return false
    }

  }

  def main(args: Array[String]): Unit = {
    try{
      val config= ConfigFactory.load("application.conf")                                                              //Load the application's configuration from the classpath resource basename

      val configuration = new Configuration()

      val logger = Logger(LoggerFactory.getLogger(config.getString("MapReduce.logger")))
      logger.info("Setting up the MapReduce job instance")

      val job = Job.getInstance(configuration, config.getString("MapReduce.jobName"))                                           //Create new instance of Job object
      logger.info("Setting up the appropriate classes for Mapper and Reducer associated with the job instance")

      job.setJarByClass(this.getClass)                                                                                                 //Set JAR to use based on class in use

      job.setMapperClass(classOf[UICAuthorsMapper])                                                                                    //Set Map and Reduce classes for the job

      job.setCombinerClass(classOf[UICAuthorsReducer])

      job.setReducerClass(classOf[UICAuthorsReducer])

      job.setInputFormatClass(classOf[XmlInputFormat])

      job.setMapOutputKeyClass(classOf[Text])                                                                                         //Set Text Object to output key
      job.setMapOutputValueClass(classOf[IntWritable])                                                                                //Set IntWritable object to output number of associations

      job.setOutputKeyClass(classOf[Text])                                                                                            //Set Text Object to output key
      job.setOutputValueClass(classOf[IntWritable])                                                                                   //Set IntWritable object to output number of associations

      logger.info("Input and output paths are provided as arguments while running the MapReduce application")

      FileInputFormat.addInputPath(job, new Path(args(config.getString("MapReduce.zero").toInt)))                             //Set input path for application

      FileOutputFormat.setOutputPath(job, new Path(args(config.getString("MapReduce.one").toInt)))                            //Set output path for application
      System.exit(if (job.waitForCompletion(true)) 0 else 1)

    } catch {
      case e: Exception => e.printStackTrace
        val config= ConfigFactory.load("application.conf")                                                             //Load the application's configuration from the classpath resource basename
        val logger = Logger(LoggerFactory.getLogger(config.getString("MapReduce.logger")))
        logger.error("The simulation has been terminated due to an unexpected error")                                                   //Log error in case of exception
    }
  }

}
